//
//  GlanceFramework.h
//  GlanceFramework
//
//  Created by Felipe Melo on 10/01/24.
//

#import <Foundation/Foundation.h>

//! Project version number for GlanceFramework.
FOUNDATION_EXPORT double GlanceFrameworkVersionNumber;

//! Project version string for GlanceFramework.
FOUNDATION_EXPORT const unsigned char GlanceFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GlanceFramework/PublicHeader.h>
#import <GlanceCore/Glance_iOS.h>
