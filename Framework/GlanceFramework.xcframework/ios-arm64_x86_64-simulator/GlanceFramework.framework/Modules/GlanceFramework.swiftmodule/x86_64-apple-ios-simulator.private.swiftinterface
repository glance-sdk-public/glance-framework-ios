// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 6.0 effective-5.10 (swiftlang-6.0.0.9.10 clang-1600.0.26.2)
// swift-module-flags: -target x86_64-apple-ios13.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -enable-bare-slash-regex -module-name GlanceFramework
// swift-module-flags-ignorable: -no-verify-emitted-module-interface
import AVFoundation
import Combine
import DeveloperToolsSupport
import Foundation
import GlanceCore
@_exported import GlanceFramework
import Swift
import SwiftUI
import UIKit
import WebKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
public struct GlanceLocalizedStringsConstants {
  public static let defaultUIPresenceDialogTitle: Swift.String
  public static let defaultUIPresenceDialogText: Swift.String
  public static let defaultUIPresenceDialogNoButtonTitle: Swift.String
  public static let defaultUIPresenceDialogYesButtonTitle: Swift.String
  public static let defaultUIPresenceDialogTermsText: Swift.String
  public static let defaultUIPresenceDialogTextVideo: Swift.String
  public static let nonDefaultUIPresenceDialogTitle: Swift.String
  public static let nonDefaultUIPresenceDialogNoButtonTitle: Swift.String
  public static let nonDefaultUIPresenceDialogYesButtonTitle: Swift.String
  public static let nonDefaultUIPresenceDialogShowTermsButtonTitle: Swift.String
  public static let defaultUIStartDialogTitle: Swift.String
  public static let defaultUIStartDialogSubtitle: Swift.String
  public static let defaultUIStartDialogTitleAgent: Swift.String
  public static let defaultUIStartDialogPhoneText: Swift.String
  public static let defaultUIStartDialogHeadsetText: Swift.String
  public static let defaultUIStartDialogDrawText: Swift.String
  public static let defaultUIStartDialogVideoText: Swift.String
  public static let defaultUIStartDialogCancelButtonTitle: Swift.String
  public static let defaultUIStartDialogAcceptButtonTitle: Swift.String
  public static let defaultUIStartDialogTermsText: Swift.String
  public static let videoDefaultUIStartDialogText: Swift.String
  public static let videoDefaultUIStartDialogCancelButtonTitle: Swift.String
  public static let videoDefaultUIStartDialogAcceptButtonTitle: Swift.String
  public static let videoDefaultUIStartDialogTermsText: Swift.String
  public static let videoDefaultUIInQueueText: Swift.String
  public static let keyboardMaskLabel: Swift.String
  public static let backgroundSuspendMaskLabel: Swift.String
  public static let backgroundPauseMaskLabel: Swift.String
  public static let defaultUIAgentCodeText: Swift.String
  public static let defaultUIInQueueText: Swift.String
  public static let defaultUICancelText: Swift.String
  public static let defaultUIVisitorEndDialogTitle: Swift.String
  public static let defaultUIVisitorEndDialogMessage: Swift.String
  public static let defaultUIVisitorEndDialogEndButton: Swift.String
  public static let defaultUIVisitorEndDialogCancelButton: Swift.String
  public static let sessionStartDialogHeaderTextAppShare: Swift.String
  public static let sessionStartDialogHeaderTextAppShareAndVideo: Swift.String
  public static let sessionStartDialogHeaderTextInQueue: Swift.String
  public static let sessionStartDialogHeaderTextInQueueCode: Swift.String
  public static let sessionStartDialogHeaderTextInSessionAddVideo: Swift.String
  public static let sessionStartDialogHeaderTextEndSession: Swift.String
  public static let sessionStartDialogDescriptionTextAppShare: Swift.String
  public static let sessionStartDialogDescriptionTextAppShareAndVideo: Swift.String
  public static let sessionStartDialogDescriptionTextInSessionAddVideo: Swift.String
  public static let sessionStartDialogAcceptButtonText: Swift.String
  public static let sessionStartDialogDeclineButtonText: Swift.String
  public static let sessionStartDialogYesButtonText: Swift.String
  public static let sessionStartDialogNoButtonText: Swift.String
  public static let sessionStartDialogTermsLinkText: Swift.String
  public static let cameraAlertTitle: Swift.String
  public static let cameraPermission: Swift.String
  public static let goToSettings: Swift.String
  public static let shareAppDialogTitle: Swift.String
  public static let shareAppDialogVideoTitle: Swift.String
  public static let shareAppDialogSubtitle: Swift.String
  public static let shareAppDialogVideoSubtitle: Swift.String
  public static let shareAppDialogPrimaryButton: Swift.String
  public static let shareAppDialogSecondaryButton: Swift.String
  public static let shareAppDialogURL: Swift.String
  public static let waitingDialogTitle: Swift.String
  public static let logoViewTitle: Swift.String
  public static let dialogHostedSessionKeyTitle: Swift.String
  public static let dialogErrorTitle: Swift.String
  public static let dialogErrorPrimaryButton: Swift.String
  public static let dialogErrorSecondaryButton: Swift.String
}
@available(iOS 13.0, *)
extension SwiftUICore.Image {
  public enum GlanceImages : Swift.String {
    case glanceAgentCursor
    case glanceClose
    case glanceConnecting
    case glanceDraw
    case glanceFlipCamera
    case glanceHangup
    case glanceHeadset
    case glanceHeadsetMan
    case glanceKeypad
    case glanceLogo
    case glanceMicrophone
    case glanceMicrophoneOff
    case glanceNewBlurOff
    case glanceNewBlurOn
    case glanceNewCamera
    case glanceCaratDown
    case glanceNewCaratUp
    case glanceNewClose
    case glanceNewDivider
    case glanceNewFlipCamera
    case glanceNewHangup
    case glanceNewHeadphones
    case glanceNewLogo
    case glanceNewMaximize
    case glanceNewMicrophoneOff
    case glanceNewMicrophoneOn
    case glanceNewMinimize
    case glanceNewSettings
    case glanceNewSpeakerphoneOff
    case glanceNewSpeakerphoneOn
    case glanceNewVideoFullscreenMinimizeDarkDark
    case glanceNewVideo_Fullscreen_Minimize_Dark
    case glanceNewVideo_Fullscreen
    case glanceNewVideoOff
    case glanceNewVideoOn
    case glanceNewVisitor
    case glancePhone
    case glanceSpeakerphone
    case glanceSpeakerphoneOff
    case glanceTabArrow
    case glanceVideo
    case glanceVideoDialogClose
    case glanceVideoInfoImage
    case noImage
    public init?(rawValue: Swift.String)
    @available(iOS 13.0, tvOS 13.0, watchOS 6.0, macOS 10.15, *)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public init(glanceImage: SwiftUICore.Image.GlanceImages)
  public func resizableFrame(width: CoreFoundation.CGFloat, height: CoreFoundation.CGFloat) -> some SwiftUICore.View
  
}
extension GlanceFramework.Glance {
  public static func setHostedSessionDelegate(_ delegate: any GlanceFramework.GlanceHostedDelegate)
  public static func startHostedSession(username: Swift.String, password: Swift.String)
  public static func stopHostedSession()
}
public enum Message : Swift.String {
  case visitorPaused
  case widgetLocation
  case widgetVisibility
  case videoSize
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public protocol MessageManagerType {
  func pause(_ isPaused: Swift.Bool)
  func widgetLocationUpdated(_ location: Swift.String)
  func widgetVisibilityUpdated(_ visibility: Swift.String)
  func visitorSizeUpdated(_ size: GlanceFramework.VideoSizeMessage)
}
public enum VideoSizeMessage : Swift.String {
  case large
  case small
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
final public class MessageManager : GlanceFramework.MessageManagerType {
  public init()
  final public func pause(_ isPaused: Swift.Bool)
  final public func widgetLocationUpdated(_ location: Swift.String)
  final public func widgetVisibilityUpdated(_ visibility: Swift.String)
  final public func visitorSizeUpdated(_ size: GlanceFramework.VideoSizeMessage)
  @objc deinit
}
@objc public class GlanceAgent : ObjectiveC.NSObject, Swift.Decodable {
  public var agentRole: Swift.String?
  public var name: Swift.String?
  public var partnerId: Swift.Int?
  public var partnerUserId: Swift.String?
  public var role: Swift.String?
  public var title: Swift.String?
  public var username: Swift.String?
  required public init(from decoder: any Swift.Decoder) throws
  @objc deinit
}

public enum BaseURLs : Swift.String {
  case settings
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
extension GlanceFramework.Glance {
  public enum SettingsInterval : Swift.Equatable {
    case never
    case always
    case repeating(Swift.Int)
    public static func == (a: GlanceFramework.Glance.SettingsInterval, b: GlanceFramework.Glance.SettingsInterval) -> Swift.Bool
  }
  public static func fetchSettingsIfNeeded(groupID: Swift.String, platform: Swift.String, visitor: Swift.String, completion: @escaping () -> Swift.Void = {})
}
@objc public class GlanceError : ObjectiveC.NSObject {
  final public let code: GlanceFramework.EventCode
  final public let message: Swift.String
  public init(code: GlanceFramework.EventCode, message: Swift.String)
  @objc deinit
}
final public class GlanceTimeout {
  public enum TimeoutType {
    case `default`, network
    public static func == (a: GlanceFramework.GlanceTimeout.TimeoutType, b: GlanceFramework.GlanceTimeout.TimeoutType) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  final public var timeInSeconds: Swift.Double
  final public var type: GlanceFramework.GlanceTimeout.TimeoutType
  public init(timeInSeconds: Swift.Double, type: GlanceFramework.GlanceTimeout.TimeoutType)
  @objc deinit
}
@objc public enum GlanceVisibility : Swift.Int, Swift.CustomStringConvertible {
  case tab = 1
  case full = 2
  public var description: Swift.String {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public enum GlanceLocation : Swift.Int, Swift.CustomStringConvertible {
  case topLeft = 1
  case topRight = 2
  case bottomLeft = 3
  case bottomRight = 4
  public var description: Swift.String {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public protocol GlanceDelegate {
  @objc func sessionConnected(dismissAction: (() -> Swift.Void)?)
  @objc @available(*, deprecated, renamed: "presenceSessionStarted")
  func presenceConnected(dismissAction: (() -> Swift.Void)?)
  @objc func receivedSessionCode(_ sessionCode: Swift.String)
  @objc func sessionAgentConnected()
  @objc func onGuestCountChange(guests: [GlanceFramework.GlanceAgent], response: Swift.String?)
  @objc func sessionEnded(_ sessionKey: Swift.String?)
  @objc func presenceDidConnect()
  @objc func presenceDidDisconnect()
  @objc func presenceSessionStarted()
  @objc func receivedVideoCaptureImage(_ image: UIKit.UIImage)
  @objc func didStartVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc func didStopVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc func didStartPreviewVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc func didStopPreviewVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc func didPauseVisitorVideo()
  @objc func didStartAgentViewer(_ glanceAgentViewer: UIKit.UIView)
  @objc func didStopAgentViewer()
  @objc func onUpdateVisibility(to visibility: GlanceFramework.GlanceVisibility)
  @objc func onUpdateLocation(to location: GlanceFramework.GlanceLocation)
  @objc func onUpdateSize(size: Swift.String)
  @objc func askForVideoSession(confirmAction: @escaping (() -> Swift.Void), cancelAction: (() -> Swift.Void)?)
  @objc func agentAskedForVideoSession()
  @objc func onUpdateAgentVideo()
  @objc func initTimeoutExpired()
  @objc func startTimeoutExpired()
  @objc func presenceTimeoutExpired()
  @objc func onEvent(_ event: GlanceEvent)
  @objc func onError(_ error: GlanceFramework.GlanceError)
}
@objc public protocol GlanceHostedDelegate {
  @objc func hostedSessionDidStart(sessionKey: Swift.String)
  @objc func hostedSessionDidEnd(errorMessage: Swift.String)
}
@_inheritsConvenienceInitializers @objc final public class Glance : ObjectiveC.NSObject {
  final public var videoSession: GlanceVideoSession?
  final public var previewCameraManager: SessionUICameraManager?
  @objc weak final public var delegate: (any GlanceFramework.GlanceDelegate)?
  @objc public static var shared: GlanceFramework.Glance
  @objc final public var initParams: GlanceVisitorInitParams?
  @objc final public var startParams: GlanceStartParams?
  @objc weak final public var hostedSessionDelegate: (any GlanceFramework.GlanceHostedDelegate)?
  @objc override dynamic public init()
  public static func getCallId() -> Swift.UInt
  public static func initVisitor(params: GlanceVisitorInitParams, timeout: Swift.Double? = nil, fetchSettingsInterval: GlanceFramework.Glance.SettingsInterval = .never, isDefaultPresenceIntegrationEnabled: Swift.Bool = true, isDefaultTwoWayVideoIntegration: Swift.Bool = true, isDefaultRenderModeIntegration: Swift.Bool = true)
  public static func startSession(params: GlanceStartParams, timeout: GlanceFramework.GlanceTimeout? = nil, maxConnectAttempts: Swift.Int? = nil)
  public static func startVideoPreview()
  public static func stopVideoPreview()
  public static func startVideoSession()
  public static func setDelegate(_ delegate: any GlanceFramework.GlanceDelegate)
  public static func endSession()
  public static func setPresence(_ isPresenceOn: Swift.Bool, timeout: Swift.Double? = nil, maxConnectAttempts: Swift.Int32? = nil)
  public static func pausePresence(isPaused: Swift.Bool)
  public static func setReplayKitEnabled(_ enabled: Swift.Bool)
  public static func setDrawViewHierarchy(_ enabled: Swift.Bool)
  public static func send(message: Swift.String, properties: Swift.String)
  public static func maskKeyboard(_ mask: Swift.Bool)
  public static func addMaskedView(_ view: UIKit.UIView, withLabel: Swift.String)
  public static func removeMaskedView(_ view: UIKit.UIView)
  public static func isPaused() -> Swift.Bool
  public static func pause(_ isPaused: Swift.Bool, message: Swift.String? = nil)
  public static func togglePause()
  @objc final public func startCaptureSession()
  @objc final public func stopCaptureSession()
  @objc deinit
}
extension GlanceFramework.Glance : GlanceVisitorDelegate {
  @objc final public func glanceVisitorEvent(_ event: GlanceEvent)
}
extension GlanceFramework.Glance : GlanceVideoSessionDelegate {
  @objc final public func glanceVideoSessionDidStart(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidStartVideoCapture(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidConnectVideoSource(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidDisconnectVideoSource(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidFailToConnectVideoSource(_ session: GlanceVideoSession!, error: (any Swift.Error)!)
  @objc final public func sessionUICameraManagerDidCapture(_ image: UIKit.UIImage!)
  @objc final public func glanceVideoSessionDidConnectStreamer(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidDisconnnectStreamer(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionWillStartStreaming(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionWillStopStreaming(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidFailConnectStreamer(_ session: GlanceVideoSession!, error: (any Swift.Error)!)
  @objc final public func glanceVideoSessionWillChangeQuality(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidEnd(_ session: GlanceVideoSession!)
  @objc final public func glanceVideoSessionInvitation(_ session: GlanceVideoSession!, sessiontype: Swift.String!, username: Swift.String!, sessionkey sesionkey: Swift.String!)
}
extension GlanceFramework.Glance : SessionUICameraManagerDelegate {
  @objc final public func sessionUICameraManagerDidStartVideoCapture(_ instance: SessionUICameraManager)
  @objc final public func sessionUICameraManagerDidStopVideoCapture(_ instance: SessionUICameraManager)
}
extension GlanceFramework.Glance : SessionUIDelegate {
  @objc final public func sessionUIVoiceAuthenticationRequired()
  @objc final public func sessionUIVoiceAuthenticationFailed()
  @objc final public func sessionUIDidError(_ error: (any Swift.Error)!)
  @objc final public func sessionUIDialogAccepted()
  @objc final public func sessionUIDialogCancelled()
}
extension GlanceFramework.Glance : GlanceCustomViewerDelegate {
  @objc final public func glanceViewerDidStart(_ glanceView: UIKit.UIView, size: CoreFoundation.CGSize)
  @objc final public func glanceViewerDidStop(_ glanceView: UIKit.UIView)
  @objc final public func glanceViewerIsStopping(_ glanceView: UIKit.UIView)
  @objc final public func glanceViewerIsStarting(_ glanceView: UIKit.UIView)
}
@objc final public class DialogsCoordinator : ObjectiveC.NSObject {
  final public let service: any GlanceFramework.GlanceServiceType
  public init(service: any GlanceFramework.GlanceServiceType = GlanceService())
  @objc final public func presentAppShareDialog()
  @objc final public func presentAppShareVideoDialog()
  @objc final public func presentAgentAskShareVideoDialog(confirmAction: (() -> Swift.Void)? = nil, cancelAction: (() -> Swift.Void)? = nil)
  @objc final public func presentWaitingAgentDialog(dismissAction: (() -> Swift.Void)?)
  @objc final public func presentConfirmationDialog(confirmAction: (() -> Swift.Void)? = nil)
  @objc final public func presentShowCodeDialog(code: Swift.String)
  @objc final public func showHostedSessionKey(code: Swift.String)
  @objc final public func presentStartDialog(confirmAction: @escaping (() -> Swift.Void), cancelAction: (() -> Swift.Void)?)
  @objc deinit
}
public protocol GlanceServiceType {
  var termsUrl: Swift.String { get }
  func setVisitorParams(_ params: GlanceVisitorInitParams, timeout: Swift.Double?)
  func setPresence(_ isPresenceOn: Swift.Bool, timeout: Swift.Double?, maxConnectAttempts: Swift.Int32)
  func startSession(params: GlanceStartParams, timeout: GlanceFramework.GlanceTimeout?, maxConnectAttempts: Swift.Int)
  func endSession()
  func startVideoPreview()
  func stopVideoPreview()
  func addVideo()
  func send(message: Swift.String, properties: Swift.String)
  func startHostedSession(username: Swift.String, password: Swift.String)
  func stopHostedSession()
}
@objc final public class GlanceService : ObjectiveC.NSObject, GlanceFramework.GlanceServiceType {
  final public var termsUrl: Swift.String
  public init(termsUrl: Swift.String = GlanceLocalizedStringsConstants.shareAppDialogURL)
  final public func setVisitorParams(_ params: GlanceVisitorInitParams, timeout: Swift.Double? = nil)
  final public func setPresence(_ isPresenceOn: Swift.Bool, timeout: Swift.Double?, maxConnectAttempts: Swift.Int32)
  final public func startSession(params: GlanceStartParams, timeout: GlanceFramework.GlanceTimeout? = nil, maxConnectAttempts: Swift.Int)
  @objc final public func endSession()
  @objc final public func startVideoPreview()
  @objc final public func stopVideoPreview()
  @objc final public func send(message: Swift.String, properties: Swift.String)
  @objc final public func addVideo()
  @objc final public func startHostedSession(username: Swift.String, password: Swift.String)
  @objc final public func stopHostedSession()
  @objc deinit
}
@available(iOS 13.0, *)
@_Concurrency.MainActor @preconcurrency public struct StartDialogButtonsView : SwiftUICore.View {
  @_Concurrency.MainActor @preconcurrency public var body: some SwiftUICore.View {
    get
  }
  @available(iOS 13.0, tvOS 13.0, watchOS 6.0, macOS 10.15, *)
  public typealias Body = @_opaqueReturnTypeOf("$s15GlanceFramework22StartDialogButtonsViewV4bodyQrvp", 0) __
}
extension SwiftUICore.View {
  @_Concurrency.MainActor @preconcurrency public func addMaskedView(withLabel label: Swift.String) -> some SwiftUICore.View
  
}
public protocol GlanceUIDelegate {
  func sessionConnected(dismissAction: (() -> Swift.Void)?)
  func receivedSessionCode(_ sessionCode: Swift.String)
  func sessionAgentConnected()
  func onGuestCountChange(guests: [GlanceFramework.GlanceAgent], response: Swift.String?)
  func sessionEnded(_ sessionKey: Swift.String?)
  func presenceDidConnect()
  func presenceDidDisconnect()
  func presenceSessionStarted()
  func receivedVideoCaptureImage(_ image: UIKit.UIImage)
  func didStartVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  func didStopVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  func didStartPreviewVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  func didStopPreviewVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  func didPauseVisitorVideo()
  func didStartAgentViewer(_ glanceAgentViewer: UIKit.UIView)
  func didStopAgentViewer()
  func onUpdateVisibility(to visibility: GlanceFramework.GlanceVisibility)
  func onUpdateLocation(to location: GlanceFramework.GlanceLocation)
  func onUpdateSize(size: Swift.String)
  func askForVideoSession(confirmAction: @escaping (() -> Swift.Void), cancelAction: (() -> Swift.Void)?)
  func agentAskedForVideoSession()
  func onUpdateAgentVideo()
  func initTimeoutExpired()
  func startTimeoutExpired()
  func presenceTimeoutExpired()
  func hostedSessionDidStart(sessionKey: Swift.String)
  func hostedSessionDidEnd(errorMessage: Swift.String)
  func onEvent(_ event: GlanceEvent)
  func onError(_ error: GlanceFramework.GlanceError)
}
@_hasMissingDesignatedInitializers final public class GlanceDefaultUI {
  public static func setVisitorParams(_ params: GlanceVisitorInitParams, timeout: Swift.Double?, maxConnectAttempts: Swift.Int = 0)
  public static func start(key: Swift.String? = nil, timeout: GlanceFramework.GlanceTimeout?, videoMode: GlanceVideoMode)
  public static func endSession()
  public static func setPresence(_ isPresenceOn: Swift.Bool, timeout: Swift.Double? = nil, maxConnectAttempts: Swift.Int32? = nil)
  public static func setDelegate(_ delegate: any GlanceFramework.GlanceUIDelegate)
  public static func startHostedSession(username: Swift.String, password: Swift.String)
  public static func stopHostedSession()
  @objc deinit
}
extension GlanceFramework.GlanceDefaultUI : GlanceFramework.GlanceDelegate, GlanceFramework.GlanceHostedDelegate {
  @objc final public func sessionConnected(dismissAction: (() -> Swift.Void)?)
  @objc final public func presenceConnected(dismissAction: (() -> Swift.Void)?)
  @objc final public func presenceDidConnect()
  @objc final public func presenceDidDisconnect()
  @objc final public func presenceSessionStarted()
  @objc final public func receivedSessionCode(_ sessionCode: Swift.String)
  @objc final public func sessionEnded(_ sessionKey: Swift.String?)
  @objc final public func sessionAgentConnected()
  @objc final public func onGuestCountChange(guests: [GlanceFramework.GlanceAgent], response: Swift.String?)
  @objc final public func receivedVideoCaptureImage(_ image: UIKit.UIImage)
  @objc final public func didStartVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc final public func didStopVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc final public func didStartPreviewVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc final public func didStopPreviewVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc final public func didPauseVisitorVideo()
  @objc final public func didStartAgentViewer(_ glanceAgentViewer: UIKit.UIView)
  @objc final public func didStopAgentViewer()
  @objc final public func onUpdateVisibility(to visibility: GlanceFramework.GlanceVisibility)
  @objc final public func onUpdateLocation(to location: GlanceFramework.GlanceLocation)
  @objc final public func onUpdateSize(size: Swift.String)
  @objc final public func askForVideoSession(confirmAction: @escaping (() -> Swift.Void), cancelAction: (() -> Swift.Void)?)
  @objc final public func agentAskedForVideoSession()
  @objc final public func onUpdateAgentVideo()
  @objc final public func initTimeoutExpired()
  @objc final public func startTimeoutExpired()
  @objc final public func presenceTimeoutExpired()
  @objc final public func onEvent(_ event: GlanceEvent)
  @objc final public func onError(_ error: GlanceFramework.GlanceError)
  @objc final public func hostedSessionDidStart(sessionKey: Swift.String)
  @objc final public func hostedSessionDidEnd(errorMessage: Swift.String)
}
public enum HTTPMethod : Swift.String {
  case GET
  case POST
  case DELETE
  case PUT
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public protocol APIFetcher {
  var path: Swift.String { get }
  var method: GlanceFramework.HTTPMethod { get }
  var task: (any Swift.Decodable & Swift.Encodable)? { get }
  var header: (any Swift.Decodable & Swift.Encodable)? { get }
  var debug: Swift.Bool { get }
}
public enum APIRequestError : Swift.Error {
  case invalidURL
  case genericError
  case parseError
  public static func == (a: GlanceFramework.APIRequestError, b: GlanceFramework.APIRequestError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
open class APIRequest {
  public func fetchRequest<T, V>(target: T, dataType: V.Type) -> Combine.AnyPublisher<V, any Swift.Error> where T : GlanceFramework.APIFetcher, V : Swift.Decodable, V : Swift.Encodable
  public init()
  @objc deinit
}
public enum SessionType {
  case screenshare, twoWayVideo, oneWayVideo, fullScreen
  public static func == (a: GlanceFramework.SessionType, b: GlanceFramework.SessionType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@_Concurrency.MainActor @preconcurrency public struct SessionAppShareDialogView_Previews : SwiftUI.PreviewProvider {
  @_Concurrency.MainActor @preconcurrency public static var previews: some SwiftUICore.View {
    get
  }
  @available(iOS 13.0, tvOS 13.0, watchOS 6.0, macOS 10.15, *)
  public typealias Previews = @_opaqueReturnTypeOf("$s15GlanceFramework34SessionAppShareDialogView_PreviewsV8previewsQrvpZ", 0) __
}
@objc final public class SessionCoordinator : ObjectiveC.NSObject {
  public init(dialogsCoordinator: GlanceFramework.DialogsCoordinator, service: any GlanceFramework.GlanceServiceType, messageManager: any GlanceFramework.MessageManagerType)
  final public func start(sessionType: GlanceFramework.SessionType = .screenshare)
  @objc deinit
}
extension SwiftUICore.View {
  @_Concurrency.MainActor @preconcurrency public func cornerRadius(_ radius: CoreFoundation.CGFloat, corners: UIKit.UIRectCorner) -> some SwiftUICore.View
  
}
public enum EventCode : Swift.Int {
  case EventNone
  case EventInvalidParameter
  case EventInvalidState
  case EventLoginSucceeded
  case EventLoginFailed
  case EventPrivilegeViolation
  case EventInvalidWebserver
  case EventUpgradeAvailable
  case EventUpgradeRequired
  case EventCompositionDisabled
  case EventConnectedToSession
  case EventSwitchingToView
  case EventStartSessionFailed
  case EventJoinFailed
  case EventSessionEnded
  case EventFirstGuestSession
  case EventTunneling
  case EventRestartRequired
  case EventConnectionWarning
  case EventClearWarning
  case EventGuestCountChange
  case EventViewerClose
  case EventActionsChange
  case EventDriverInstallError
  case EventRCDisabled
  case EventDeviceDisconnected
  case EventDeviceReconnected
  case EventGesture
  case EventException
  case EventScreenshareInvitation
  case EventMessageReceived
  case EventChildSessionStarted
  case EventChildSessionEnded
  case EventJoinChildSessionFailed
  case EventVisitorInitialized
  case EventPresenceConnected
  case EventPresenceConnectFail
  case EventPresenceShowTerms
  case EventPresenceStartSession
  case EventPresenceSignal
  case EventPresenceBlur
  case EventPresenceSendFail
  case EventPresenceNotConfigured
  case EventPresenceDisconnected
  case EventPresenceStartVideo
  case EventVisitorVideoRequested
  public init(fromRawValue: Swift.UInt)
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum EventType : Swift.Int {
  case none
  case info
  case warning
  case error
  case assertFail
  public init(fromRawValue: Swift.UInt)
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public struct GlanceSDKEvent {
  public var code: GlanceFramework.EventCode
  public var type: GlanceFramework.EventType
  public var message: Swift.String
  public var properties: [Swift.AnyHashable : Any]
  public init(code: GlanceFramework.EventCode, type: GlanceFramework.EventType, message: Swift.String, properties: [Swift.AnyHashable : Any])
  public init(_ object: GlanceEvent)
}
@available(iOS 13.0, *)
extension SwiftUICore.Image.GlanceImages : Swift.Equatable {}
@available(iOS 13.0, *)
extension SwiftUICore.Image.GlanceImages : Swift.Hashable {}
@available(iOS 13.0, *)
extension SwiftUICore.Image.GlanceImages : Swift.RawRepresentable {}
extension GlanceFramework.Message : Swift.Equatable {}
extension GlanceFramework.Message : Swift.Hashable {}
extension GlanceFramework.Message : Swift.RawRepresentable {}
extension GlanceFramework.VideoSizeMessage : Swift.Equatable {}
extension GlanceFramework.VideoSizeMessage : Swift.Hashable {}
extension GlanceFramework.VideoSizeMessage : Swift.RawRepresentable {}
extension GlanceFramework.BaseURLs : Swift.Equatable {}
extension GlanceFramework.BaseURLs : Swift.Hashable {}
extension GlanceFramework.BaseURLs : Swift.RawRepresentable {}
extension GlanceFramework.GlanceTimeout.TimeoutType : Swift.Equatable {}
extension GlanceFramework.GlanceTimeout.TimeoutType : Swift.Hashable {}
extension GlanceFramework.GlanceVisibility : Swift.Equatable {}
extension GlanceFramework.GlanceVisibility : Swift.Hashable {}
extension GlanceFramework.GlanceVisibility : Swift.RawRepresentable {}
extension GlanceFramework.GlanceLocation : Swift.Equatable {}
extension GlanceFramework.GlanceLocation : Swift.Hashable {}
extension GlanceFramework.GlanceLocation : Swift.RawRepresentable {}
@available(iOS 13.0, *)
extension GlanceFramework.StartDialogButtonsView : Swift.Sendable {}
extension GlanceFramework.HTTPMethod : Swift.Equatable {}
extension GlanceFramework.HTTPMethod : Swift.Hashable {}
extension GlanceFramework.HTTPMethod : Swift.RawRepresentable {}
extension GlanceFramework.APIRequestError : Swift.Equatable {}
extension GlanceFramework.APIRequestError : Swift.Hashable {}
extension GlanceFramework.SessionType : Swift.Equatable {}
extension GlanceFramework.SessionType : Swift.Hashable {}
extension GlanceFramework.SessionAppShareDialogView_Previews : Swift.Sendable {}
extension GlanceFramework.EventCode : Swift.Equatable {}
extension GlanceFramework.EventCode : Swift.Hashable {}
extension GlanceFramework.EventCode : Swift.RawRepresentable {}
extension GlanceFramework.EventType : Swift.Equatable {}
extension GlanceFramework.EventType : Swift.Hashable {}
extension GlanceFramework.EventType : Swift.RawRepresentable {}
