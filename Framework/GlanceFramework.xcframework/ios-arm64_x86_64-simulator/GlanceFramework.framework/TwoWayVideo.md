#  TwoWayVideo - Step by Step Guide

## Summary
1. [Setup](#setup)
2. [Configure Session](#configure-session)
3. [Starting a Session](#starting-a-session)

## Setup

#### Via XCode
1. In Xcode, install **GlanceDefaultUI** by navigating to **File** > **Add Packages**.
2. In the prompt that appears, select the **GlanceDefaultUI** repository:
```
https://gitlab.com/glance-sdk-public/glance-default-ui-ios-releases
```
3. Select the version of **glance-sdk-ios-releases** you want to use. For new projects, we recommend using the newest version.

#### Via Package.swift
To integrate **GlanceDefaultUI** to a Swift package via a **Package.swift** manifest, you can add **GlanceDefaultUI** to the dependencies array of your package.
```
dependencies: [

  .package(name: "GlanceDefaultUI",
           url: "https://gitlab.com/glance-sdk-public/glance-default-ui-ios-releases.git",
           from: "6.6.0"),
  // ...

],
```
## Configure Session
Call `GlanceDefaultUI.setVisitorParams` to configure the account parameters:
```swift
import GlanceSDK
import GlanceDefaultUI
.
.
let params = GlanceVisitorInitParams()
params.groupid = 2134
params.visitorid = "glance"
params.video = true // Set the Two Way Video session, assign false to start screenshare

GlanceDefaultUI.setVisitorParams(params)
```

## Starting a session
Call `start` with the desired key.

```swift
let sessionKey = "7978"
GlanceDefaultUI.start(key: sessionKey)
```

### Start random session
To start random sessions, do not provide a `visitorid` on the configuration step.
```swift
.
.
GlanceDefaultUI.start()
.
.
```

### End session
```swift
GlanceDefaultUI.endSession()
```
