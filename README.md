# GlanceFramework

Framework containing both `GlanceDefaultUI` and `GlanceSDK`.
- [GlanceDefaultUI Documentation](https://gitlab.com/glance-sdk-public/glance-default-ui-ios-releases)
- [GlanceSDK Documentation](https://gitlab.com/glance-sdk-public/glance-sdk-ios-releases)

## Installation

`GlanceFramework` can only be integrated manually by following the steps below.

#### Download
1. Download `GlanceFramework` on this repo.
2. Move `Framework` folder into your project.

#### Add framework
With your project openned, drag the following xcframeworks into your "Frameworks, Libraries and Embedded Content" settings section and select "Embed & Sign":
- `GlanceCore.xcframework`
- `GlanceFramework.xcframework`

## Usage
`GlanceFramework` has both `GlanceDefaultUI` and `GlanceSDK`, so the usage is mainly the same according with each one's docs.

The only difference is that instead of importing the following packages.
```
import GlanceSDK
import GlanceSDKSwift
import GlanceDefaultUI
```
It's only required to import `GlanceFramework`.
```
import GlanceFramework
```
## Example
For any doubts, check 'Test' example project within this repo.
